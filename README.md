# Nooijen Web Solutions WP Framework
_The Nooijen Web Solutions WordPress framework_

This framework is a collection of existing premium and free plugins and a child theme of GeneratePress with lots of customisation options to it. This framework was made for two reasons: giving the Nooijen Web Solutions developers more room to play with, while keeping everything client-friendly _(some might call this dummy-proof)_. This framework is still in development (as developers we will always see room for improvements), but many features have been added already.

### Theme
This framework is based on the GeneratePress theme (GeneratePress Premium, this is one of the premium plugins). A child theme is added with customisations to keep everything future-proof and resistant to updates. In the functions.php you can enable and disable the options, and you can tweak it further in the child theme's /inc/custom-theme.php file.

### Premium plugins
_not included in this repo because of copyright stuff_
* Elementor Pro
* GeneratePress Premium
* Gravity Forms
* WPML (optional)

### Production & development plugins
_install using cli or install them one by one if you want to play on hardcore mode_
* WP Security
* Autoptimize
* BackWPup 
* Broken Link Checker
* Duplicate Post
* Elementor
* Enable media replace
* Jetpack 
* Regenerate Thumbnails 
* Widget Css Classes
* Yoast SEO
* WP Super Cache 

### Dev only plugins
* Duplicator
* Ewww Image optimizer
* Fakerpress
* Pixabay Images
* Query monitor
* Show Current Template
* WP Clean Up 

## Installing (fresh WordPress install)
To install this framework there are three simple steps. First run the WP CLI command below, secondly, install the premium plugins and activate them. And lastly, add the child theme included in this repo to the `/wp-content/themes/` folder and activate the this child theme. It's as easy as that! If you like, you can also import the GeneratePress settings JSON files to save some time.

### CLI commands
WordPress CLI commands for using this framework

#### Installing plugins on setup
```
wp theme install generatepress && wp plugin install all-in-one-wp-security-and-firewall autoptimize backwpup broken-link-checker duplicate-post duplicator elementor enable-media-replace ewww-image-optimizer fakerpress jetpack pixabay-images query-monitor regenerate-thumbnails show-current-template widget-css-classes wordpress-seo wp-clean-up wp-super-cache --activate
```

#### Set development environment to production
```
wp plugin delete duplicator ewww-image-optimizer fakerpress pixabay-images query-monitor show-current-template wp-clean-up
```

## Coming features
There are a few features we as a team would like to add to this framework (this list gets updated on a regular basis). A list is added below:
* Adding ready-to-go frequently used Gravity Forms forms
* Adding some ready-to-go custom Elementor (Pro) templates
* Making everything a but more organized in the child theme folder
* Adding a custom WP-Admin theme (Material Design like?)